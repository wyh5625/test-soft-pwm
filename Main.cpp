#include <iostream>
#ifdef __cplusplus
extern "C" {
#endif
#include <unistd.h>
#include <sys/time.h>
#ifdef __cplusplus
}
#endif

#include "ServoClass.hpp"
#include "LEDClass.hpp"
#include "MotorEncoder.hpp"
using namespace std;

/*
 * 
 */

int servo(int argc, char** argv) {
    ServoClass servo(4);

    //servo.setAngle(0);
    servo.setDutyCyclePercentage(3.5);
    servo.delay(500);
    servo.setAngle(90);
    servo.delay(500);
    //servo.setAngle(180);
    servo.setDutyCyclePercentage(15.5);
    servo.delay(500);

    for (int i = 0; i < 180; i++) {
        servo.setAngle(i);
        servo.delay(50);
    }
    return 0;
}

int led(int argc, char** argv) {
    LEDClass led(23);

    int i;
    for (i = 0; i <= 60; i++) {
        led.setDutyCyclePercentage(i);
        led.delay(50);
    }
    for (; i >= 0; i--) {
        led.setDutyCyclePercentage(i);
        led.delay(50);
    }
    sleep(2);

}

int motorDriver(int argc, char** argv) {
    MotorDriver motor(4, 17, 18, 22, 23);
    motor.forward();
    sleep(3);
    motor.setDutyCyclePercentage(50);
    motor.backward();
    sleep(3);
    motor.stop();
}

inline long millis() {
    struct timeval millis;
    gettimeofday(&millis, NULL);
    return (millis.tv_usec + millis.tv_sec * 1000000) / 1000;
}

#define TIME_INTERVAL 2000

long motorEncoder(int argc, char** argv) {
    unsigned long cur_time, prev_time;
    long prev_counter, counter;
    MotorEncoder motor(4, // pwm gpio
            17, //motor driver a1 gpio
            18, //motor driver a2 gpio
            22, //motor driver b1 gpio,
            23, //motor driver b2 gpio,
            20, //motor encoder input-a gpio,
            21 //motor encoder input-b gpio
            );

    motor.forward();
    prev_time = millis();
    prev_counter = -99;
    do {
        counter = motor.readCounter();
        if (counter != prev_counter) std::cout << counter << "\r\n";
        prev_counter=counter;
    } while ((cur_time = millis()) < prev_time + TIME_INTERVAL);

    motor.stop();
}

int main(int argc, char** argv) {
    motorEncoder(argc, argv);
    //motorDriver(argc,argv);
    //led(argc,argv);
    //servo(argc,argv);
}