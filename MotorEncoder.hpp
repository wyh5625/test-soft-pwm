/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotorEncoder.hpp
 * Author: user
 *
 * Created on March 22, 2017, 7:38 PM
 */

#ifndef MOTOR_ENCODER_HPP
#define MOTOR_ENCODER_HPP
#include "MotorDriver.hpp"

class MotorEncoder : public MotorDriver {
public:
    MotorEncoder();
    MotorEncoder(const MotorEncoder& orig);
    MotorEncoder(int pwmGpio, int a1_gpio, int a2_gpio, int b1_gpio, int b2_gpio, int outputA_gpio, int outputB_gpio);
    long readCounter();
    virtual ~MotorEncoder();
private:
    GPIOClass outputA;
    GPIOClass outputB;
    volatile long counter;
    int state;
    int lastState;

};

#endif /* MOTOR_ENCODER_HPP */

